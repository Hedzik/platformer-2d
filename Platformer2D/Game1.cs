﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Design;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System;
using System.Diagnostics;

public static class Texture2DExtensions
{
    public static Color[,] To2DArray(this Texture2D texture)
    {
        Color[] colors1D = new Color[texture.Width * texture.Height];
        texture.GetData(colors1D);
        Color[,] colors2D = new Color[texture.Width, texture.Height];

        for (int x = 0; x < texture.Width; x++)
            for (int y = 0; y < texture.Height; y++)
                colors2D[x, y] = colors1D[x + y * texture.Width];

        return colors2D;
    }
}

namespace Platformer2D
{

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class PlatformerGame : Game
    {
        public GraphicsDeviceManager Graphics { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public TilemapDisplayAndBehaviourManager Tilemap { get; private set; }
        private Texture2D background;

        public PlatformerGame()
        {
            Graphics = new GraphicsDeviceManager(this); // 480 * 800 -> 30 * 50
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Tilemap = new TilemapDisplayAndBehaviourManager(this, 16);
            Components.Add(Tilemap);

            Components.Add(new LocalPlayer(this, new PlayerInput
            {
                Right = Keys.D,
                Left = Keys.A,
                Jump = Keys.Space
            }));

            base.Initialize();
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            // TODO: use this.Content to load your game content here

            Texture2D sourceTilemap = Content.Load<Texture2D>("Levels/level_0");
            Texture2D defaultBox = Content.Load<Texture2D>("Tiles/wooden_box");
            Texture2D coinTile = Content.Load<Texture2D>("Tiles/coin");
            IEnumerable<Tile> tiles = TextureToTilemap.Parse(sourceTilemap, 16, (pixel, position) =>
              {
                  if (pixel == Color.Black)
                      return new Tile
                      {
                          Texture = defaultBox,
                          TileType = TileType.DefaultBox
                      };
                  else if (pixel == new Color(255, 127, 39)) //The default MS paint's orange
                      return new Tile
                      {
                          Texture = coinTile,
                          TileType = TileType.Coin,
                          IsAPlatform = false
                      };
                  return null;
              });
            Tilemap.Setup(tiles);

            background = Content.Load<Texture2D>("Backgrounds/cave");

            Texture2D snakeAnimations = Content.Load<Texture2D>("Character/characters").Substract(GraphicsDevice, new Rectangle(0, 96, 128, 32));
            Components.Add(new Snake(this,snakeAnimations, new Vector2(17 * 16 - 16, 25 * 16 - 16), new Vector2(22 * 16 - 16, 25 * 16 - 16), Direction.Right)
            {
                Speed = 60f
            });
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            base.Update(gameTime);

            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            SpriteBatch.Begin();

            //Draw background...
            SpriteBatch.Draw(background, new Rectangle(0, 0, Graphics.PreferredBackBufferWidth, Graphics.PreferredBackBufferHeight), Color.White);

            base.Draw(gameTime);

            SpriteBatch.End();

        }
    }
}
