﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    /// <summary>
    /// A helper class to check rectangle collisions.
    /// </summary>
    internal static class RectangleHelper
    {
        const int penetrationMargin = 5;
        public static bool IsOnTopOf(this Rectangle r1, Rectangle r2)
        {
            
            return r1.Intersects(r2);
        }

        //Rect gracza. Sprawdzic czy sie zderzaja. Dostac ten zderzenie rectangle. Sprawdzic czy jest na gorze w sotsunku do platformy.

        public static bool IsOnTheTopOf(Rectangle first, Rectangle second)
        {
            Rectangle.Intersect(ref first, ref second, out Rectangle collisionArea);
            return first.Bottom > second.Top && first.Bottom < second.Bottom;
        }
    }
}
