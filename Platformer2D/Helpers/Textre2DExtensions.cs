﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Threading.Tasks;
namespace Platformer2D
{
    public static class Textre2DExtensions
    {
        /// <summary>
        /// Substracts new <see cref="Texture2D"/> using given <paramref name="area"/>.
        /// </summary>
        /// <returns>The substracted <see cref="Texture2D"/></returns>
        /// <param name="graphicsDevice">Graphics device</param>
        /// <param name="area">The area of new <see cref="Texture2D"/>.</param>
        public static Texture2D Substract(this Texture2D originalTexture, GraphicsDevice graphicsDevice, Rectangle area)
		{
            Texture2D cropTexture = new Texture2D(graphicsDevice, area.Width, area.Height);
            Color[] data = new Color[area.Width * area.Height];
            originalTexture.GetData(0, area, data, 0, data.Length);
            cropTexture.SetData(data);
            return cropTexture;
		}
	}

    
}
