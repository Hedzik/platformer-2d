﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public class LevelManager : DrawableGameComponent
    {
        public new readonly PlatformerGame Game;

        public List<BaseEnemy> Enemies = new List<BaseEnemy>();
        public List<Tile> Tiles;

        public LocalPlayer Player;

        public LevelManager(Game game) : base(game)
        {
            this.Game = game as PlatformerGame;
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var enemy in Enemies)
                enemy.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (var enemy in Enemies)
                enemy.Draw(gameTime);

            foreach (var tile in Tiles)
                tile.Draw(Game.SpriteBatch, gameTime);

            Player.Draw(gameTime);
        }

        public virtual void GenerateWorld()
        {

        }

    }

    public class FirstLevel : LevelManager
    {
        public FirstLevel(Game game) : base(game)
        {
        }

        public override void GenerateWorld()
        {
            Texture2D sourceTilemap = Game.Content.Load<Texture2D>("Levels/level_0");
            Texture2D defaultBox = Game.Content.Load<Texture2D>("Tiles/wooden_box");
            Texture2D coinTile = Game.Content.Load<Texture2D>("Tiles/coin");
            IEnumerable<Tile> tiles = TextureToTilemap.Parse(sourceTilemap, 16, (pixel, position) =>
            {
                if (pixel == Color.Black)
                    return new Tile
                    {
                        Texture = defaultBox,
                        TileType = TileType.DefaultBox
                    };
                else if (pixel == new Color(255, 127, 39)) //The default MS paint's orange
                    return new Tile
                    {
                        Texture = coinTile,
                        TileType = TileType.Coin,
                        IsAPlatform = false
                    };
                return null;
            });

            this.Tiles = tiles.ToList();

            this.Player = new LocalPlayer(Game, new PlayerInput
            {
                Right = Keys.D,
                Left = Keys.A,
                Jump = Keys.Space
            });

            //Texture2D snakeAnimations = Game.Content.Load<Texture2D>("Character/characters").Substract(GraphicsDevice, new Rectangle(0, 96, 128, 32));
            //Enemies.Add(
            //    new Snake(snakeAnimations, new Vector2(32, 100), new Vector2(100, 100), Direction.Right)
            //{
            //    Speed = 45f
            //});
        }
    }
}
