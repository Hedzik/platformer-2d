﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer2D.GraphicsDebug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public enum TileType
    {
        DefaultBox,
        Coin
    }

    public class Tile
    {
        public Vector2 Position { get; set; }
        public Point TilemapPosition { get; set; }
        public Texture2D Texture { get; set; }
            
        public Point Dimensions { get; set; }
        public Rectangle Hitbox { get; set; }

        public TileType TileType { get; set; }

        public bool IsAPlatform { get; set; } = true;


        public Tile() { }

        public Tile(Texture2D texture)
        {
            Texture = texture;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
            => spriteBatch.Draw(Texture, Hitbox, Color.White);
        

    }

    public static class TextureToTilemap
    {
        public static IEnumerable<Tile> Parse(Texture2D texture, int tileDimensions, Func<Color, Point, Tile> parser)
        {
            Color[,] pixels = texture.To2DArray();
            Tile parserResult;
            for (int x = 0; x < pixels.GetLength(0); x++)
            {
                for (int y = 0; y < pixels.GetLength(1); y++)
                {
                    parserResult = parser(pixels[x, y], new Point(x, y));
                    if(parserResult != null)
                    {
                        parserResult.TilemapPosition = new Point(x, y);
                        parserResult.Position = new Vector2(x * tileDimensions, y * tileDimensions);
                        parserResult.Dimensions = new Point(tileDimensions, tileDimensions);
                        parserResult.Hitbox = new Rectangle(parserResult.Position.ToPoint(), parserResult.Dimensions);
                        yield return parserResult;
                    }
                        
                }
            }
        }
    }

    class Platform
    {

    }

    public class TilemapDisplayAndBehaviourManager : DrawableGameComponent
    {
        private readonly PlatformerGame game;
        private readonly int tileDimensions;

        public List<Tile> Tiles { get; set; }

        public TilemapDisplayAndBehaviourManager(Game game, int tileDimensions) : base(game)
        {
            this.game = game as PlatformerGame;
            this.tileDimensions = tileDimensions;
        }

        /// <summary>
        /// TIP: to get tiles from image you can use <see cref="TextureToTilemap"/> converter.
        /// </summary>
        public void Setup(IEnumerable<Tile> tiles)
            => Tiles = tiles.ToList();

        public void Setup(List<Tile> tiles)
            => Tiles = tiles;

        public override void Draw(GameTime gameTime)
        {
            foreach (var tile in Tiles)
            {
                tile.Draw(game.SpriteBatch, gameTime);
            }           
               
            
        }
    }
}
