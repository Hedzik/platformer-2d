﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D.TilesetSystem
{
    abstract class BaseTile
    {

    }

    public class LayerParser
    {
        /*In general: 
            - get image
            - scan each pixel
            - each pixel is associated with different texture (a tile) which is defined in the tilemap.

            TODO:
                - Load image as 2d array of pixels
                - Some kind of parser over each pixel
                - get result as 2d array of somekind of class which represents a tile
         */

        private Texture2D currrentLayer;
        private Color[,] pixels;

        public void LoadLayer(Texture2D layer)
        {
            currrentLayer = layer;
            pixels = currrentLayer.To2DArray();
        }
        
        
    }
}
