﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public abstract class BaseEnemy : DrawableGameComponent
    {
        protected new readonly PlatformerGame Game;

        public BaseEnemy(Game game) 
            : base(game)
        {
            this.Game = game as PlatformerGame;
        }

        public int HP { get; set; }
        public int MaxHP { get; }
    }
}
