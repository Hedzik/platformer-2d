﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public class Snake : BaseEnemy
    {
        public Vector2 Position;
        public Vector2 Velocity;

        public float Speed = 200;

        private Vector2 startPoint;
        private Vector2 endPoint;

        private float trackProgress = 0f;

        private float timer = 0f;
        private float TrackDuration
            => Vector2.Distance(startPoint, endPoint) / Speed;

        private readonly AnimationManager animationManager;
        private readonly Animation moveLeftAnimation;
        private readonly Animation moveRightAnimation;

        private Direction currentDirection;

        public Snake(Game game, Texture2D animationTexture, Vector2 startPoint, Vector2 endPoint, Direction initialDirection) 
            : base(game)
        {
            this.startPoint = startPoint;
            this.endPoint = endPoint;

            moveRightAnimation = new Animation
            {
                Texture = animationTexture,
                Frames = 4,
                FrameDuration = 200,
                FrameWidth = 32,
                FrameHeight = 32,
                IsLooping = true
            };

            moveLeftAnimation = new Animation
            {
                Texture = animationTexture,
                Frames = 4,
                FrameDuration = 200,
                FrameWidth = 32,
                FrameHeight = 32,
                Effects = SpriteEffects.FlipHorizontally,
                IsLooping = true
            };
            currentDirection = initialDirection;
            animationManager = new AnimationManager
            {
                CurrentAnimation = currentDirection == Direction.Left ? moveLeftAnimation : moveRightAnimation
            };
            animationManager.Play();
            
        }

        public override void Update(GameTime gameTime)
        {
            if (timer >= TrackDuration)
            {
                ChangeDirection();
                timer = 0f;
            }
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            trackProgress = timer / TrackDuration;

            Position = Vector2.Lerp(startPoint, endPoint, trackProgress);
        }

        private void ChangeDirection()
        {
            Vector2 oldStartPoint = startPoint;
            startPoint = endPoint;
            endPoint = oldStartPoint;


            currentDirection = currentDirection == Direction.Left ? Direction.Right : Direction.Left;
            animationManager.Stop();
            animationManager.CurrentAnimation = currentDirection == Direction.Left ? moveLeftAnimation : moveRightAnimation;
            animationManager.Play();
        }

        public override void Draw(GameTime gameTime)
            => animationManager.Draw(Game.SpriteBatch, gameTime, Position);


    }
}
