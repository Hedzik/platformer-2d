﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public enum AnimationFramesDirection
    {
        /// <summary>
        /// From right to the left
        /// </summary>
        ToTheLeft,
        /// <summary>
        /// From left to the right
        /// </summary>
        ToTheRight,
        /// <summary>
        /// From the top to the bottom
        /// </summary>
        ToTheBottom,
        /// <summary>
        /// From the bottom to the top
        /// </summary>
        Up
    }

    public class Animation
    {
        /// <summary>
        /// The image with the animation on it.
        /// </summary>
        public Texture2D Texture { get; set; }

        /// <summary>
        /// Describes for how long each frame should be displayed. (In miliseconds)
        /// </summary>
        public float FrameDuration { get; set; }

        /// <summary>
        /// The total amount of frames to play.
        /// </summary>
        public int Frames { get; set; }

        /// <summary>
        /// The diretion of frames on the texture. Typically set to <see cref="AnimationFramesDirection.ToTheLeft"/>
        /// Currently not supported yet, because im too lazy :) (It goes from right to left)
        /// </summary>
        public AnimationFramesDirection FrameDirection { get; set; } = AnimationFramesDirection.ToTheLeft;

        /// <summary>
        /// Width of each frame on the <see cref="Texture"/>
        /// </summary>
        public int FrameWidth { get; set; }
        /// <summary>
        /// Height of each frame on the <see cref="Texture"/>
        /// </summary>
        public int FrameHeight { get; set; }

        /// <summary>
        /// The point in the texture where we gonna start the animation
        /// </summary>
        public Point TextureStartPoint { get; set; } = Point.Zero;

        /// <summary>
        /// Might be usefull when it comes to make for example movement animation. 
        /// You can basically have one animation for moving right and use the same one but reversed horizonatlly as the moving left animation.
        /// </summary>
        public SpriteEffects Effects { get; set; } = SpriteEffects.None;

        /// <summary>
        /// Determines if the animation should loop itself untill we manually don't stop it
        /// </summary>
        public bool IsLooping { get; set; } = false;
    }
}
