﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public class AnimationManager
    {
        public Animation CurrentAnimation { get; set; }

        private Vector2 position;
        private float timer = 0;
        private int elapsedFrames = 0;
        public bool IsPlaying { get; private set; } = false;

        public AnimationManager()
        {

        }

        public AnimationManager(Animation initialAnimation)
        {
            CurrentAnimation = initialAnimation;
            IsPlaying = true;
        }

        /// <summary>
        /// Plays the animation.
        /// </summary>
        public void Play()
            => IsPlaying = CurrentAnimation != null;
        /// <summary>
        /// Stops the animation.
        /// </summary>
        public void Stop()
            => IsPlaying = false;

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (CurrentAnimation == null)
                return;

            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedFrames >= CurrentAnimation.Frames - 1) //The animation has ended (The Frames is the amount of frames to play so to get index we need to subtract 1)
            {
                elapsedFrames = 0;
                if(!CurrentAnimation.IsLooping)
                    IsPlaying = false;
            }

            if (timer >= CurrentAnimation.FrameDuration) // we should display next frame of the animation
            {
                timer = 0f;
                elapsedFrames++;
            }

            if (IsPlaying)
                spriteBatch.Draw(
                    CurrentAnimation.Texture,
                    position,
                    new Rectangle(CurrentAnimation.TextureStartPoint.X + elapsedFrames * CurrentAnimation.FrameWidth,
                                  CurrentAnimation.TextureStartPoint.Y,
                                  CurrentAnimation.FrameWidth,
                                  CurrentAnimation.FrameHeight),
                    Color.White,0f,Vector2.Zero, 1f,CurrentAnimation.Effects, 1f);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Vector2 position)
        {
            this.position = position;
            Draw(spriteBatch, gameTime);
        }
    }
}
