﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public class LocalPlayerGraphicsManager
    {
        private readonly LocalPlayer player;

        public AnimationManager AnimationManager { get; set; }
        public Animation MoveRightAnimation { get; set; }
        public Animation MoveLeftAnimation { get; set; }
        public Animation JumpRightAnimation { get; set; }
        public Animation JumpLeftAnimation { get; set; }
        public Animation AttackAnimation { get; set; }

        /// <summary>
        /// TODO: Make an "stand" animation
        /// </summary>
        private Texture2D TMP_sourceAnimationTexture;

        public LocalPlayerGraphicsManager(LocalPlayer player, Texture2D TMP_sourceAnimationTexture)
        {
            this.player = player;
            this.TMP_sourceAnimationTexture = TMP_sourceAnimationTexture;
        }



        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (AnimationManager.IsPlaying)
            {
                AnimationManager.Draw(spriteBatch, gameTime, player.Position);
                return;
            }

            //It's kinda "dirty" way of doing that but since i don't have any "stand" animation imma just display the character turned right or left (static image)
            //And because of that i need to somehow get the texture
            //So im getting it by one of the already set up animations
            //Yeah... I really should have picked a better sprite sheet
            if (player.movementManager.characterDirection == Direction.Left)
                spriteBatch.Draw(
                    TMP_sourceAnimationTexture,
                    player.Position,
                    new Rectangle(0,
                                  64,
                                  32,
                                  32),
                    Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.FlipHorizontally, 1f);
            else
                spriteBatch.Draw(
                    TMP_sourceAnimationTexture,
                    player.Position,
                    new Rectangle(0,
                                  64,
                                  32,
                                  32),
                    Color.White);

        }

        public void PlayAnimation(Animation animation)
        {
            AnimationManager.Stop();
            AnimationManager.CurrentAnimation = animation;
            AnimationManager.Play();
        }

        public void PlayMoveLeftAnimation()
            => PlayAnimation(MoveLeftAnimation);

        public void PlayMoveRightAnimation()
            => PlayAnimation(MoveRightAnimation);

        public void PlayJumpLeftAnimation()
            => PlayAnimation(JumpLeftAnimation);

        public void PlayJumpRightAnimation()
            => PlayAnimation(JumpRightAnimation);

    }
}
