﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platformer2D.GraphicsDebug;

namespace Platformer2D
{

    public class LocalPlayerCombatManager
    {
        private readonly LocalPlayer player;

        public void Update(GameTime gameTime)
        {
            if(Keyboard.GetState().IsKeyDown(player.Input.Attack))
            {
                player.graphicsManager.PlayAnimation(player.graphicsManager.AttackAnimation);
            }
        }


        public LocalPlayerCombatManager(LocalPlayer player)
        {
            this.player = player;
        }


    }

    public class PlayerInput
    {
        public Keys Right { get; set; }
        public Keys Left { get; set; }
        public Keys Jump { get; set; }
        public Keys Attack { get; set; } = Keys.J;
    }

    public enum Direction
    {
        Left,
        Right
    }


    public partial class LocalPlayer : DrawableGameComponent
    {
        internal readonly PlatformerGame game;
        internal LocalPlayerGraphicsManager graphicsManager;
        internal LocalPlayerCombatManager combatManager;
        internal LocalPlayerMovementManager movementManager;
        /// <summary>
        /// Player's width.
        /// </summary>
        public const int WIDTH = 32;

        /// <summary>
        /// Player's height.
        /// </summary>
        public const int HEIGHT = 32;


        public readonly PlayerInput Input;

        /// <summary>
        /// Player's current position.
        /// </summary>
        public Vector2 Position = new Vector2(250, 250);

        /// <summary>
        /// The velocity is the force which is aded to <see cref="Position"/> each frame.
        /// Allows for a lot of  movement, jumping
        /// </summary>
        public Vector2 Velocity;


        /// <summary>
        /// Player's hitbox.
        /// </summary>
        public Rectangle Hitbox
            => new Rectangle(Position.ToPoint(), new Point(WIDTH, HEIGHT));



        public LocalPlayer(Game game, PlayerInput input) : base(game)
        {
            this.game = game as PlatformerGame;
            this.Input = input;
        }

        protected override void LoadContent()
        {
            Texture2D characterAnimationsTexture = game.Content.Load<Texture2D>("Character/characters");
            Texture2D moveAnimation = characterAnimationsTexture.Substract(game.GraphicsDevice, new Rectangle(0, 64, 128, 32));

            Texture2D swooshAnimationTexture = game.Content.Load<Texture2D>("Character/swoosh");
            graphicsManager = new LocalPlayerGraphicsManager(this, characterAnimationsTexture)
            {
                AnimationManager = new AnimationManager(),
                MoveRightAnimation = new Animation
                {
                    FrameDuration = 100,
                    Frames = 4,
                    FrameWidth = WIDTH,
                    FrameHeight = HEIGHT,
                    Texture = moveAnimation
                },
                MoveLeftAnimation = new Animation
                {
                    FrameDuration = 100,
                    Frames = 4,
                    FrameWidth = WIDTH,
                    FrameHeight = HEIGHT,
                    Texture = moveAnimation,
                    Effects = SpriteEffects.FlipHorizontally
                },
                JumpRightAnimation = new Animation
                {
                    FrameDuration = 50,
                    Frames = 4,
                    FrameWidth = WIDTH,
                    FrameHeight = HEIGHT,
                    TextureStartPoint = new Point(128, 64),
                    Texture = characterAnimationsTexture
                },
                JumpLeftAnimation = new Animation
                {
                    FrameDuration = 50,
                    Frames = 4,
                    FrameWidth = WIDTH,
                    FrameHeight = HEIGHT,
                    TextureStartPoint = new Point(128, 64),
                    Texture = characterAnimationsTexture,
                    Effects = SpriteEffects.FlipHorizontally
                },
                AttackAnimation = new Animation
                {
                    FrameDuration = 50,
                    Frames = 4,
                    FrameWidth = WIDTH,
                    FrameHeight = HEIGHT,
                    Texture = swooshAnimationTexture,
                }
            };

            combatManager = new LocalPlayerCombatManager(this);

            movementManager = new LocalPlayerMovementManager(this);
        }

        public override void Update(GameTime gameTime)
        {
            CheckForInput(gameTime);

            combatManager.Update(gameTime);

            Position += Velocity;
        }

        private void CheckForInput(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;


            movementManager.HandleJumping(keyboardState, deltaTime);

            movementManager.HandleHerizontalMovement(keyboardState, deltaTime);

        }




        public override void Draw(GameTime gameTime)
        {
            graphicsManager.Draw(game.SpriteBatch, gameTime);
            //game.DrawBorder(Hitbox, 1, Color.Red); //debug purposes
        }



    }
}
