﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer2D
{
    public class LocalPlayerMovementManager
    {
        private readonly LocalPlayer player;
        private readonly PlatformerGame game;

        /// <summary>
        /// The current character's direction. Helper field for picking up right animation. 
        /// </summary>
        internal Direction characterDirection;

        /// <summary>
        /// The acceleration of movement.
        /// Notice it's going to additionally multiply by delta time.
        /// </summary>
        public float MovementAcceleration = 15;
        /// <summary>
        /// Height of the jump.
        /// Notice it's going to additionally multiply by delta time.
        /// </summary>
        public float JumpHeight = 1200;

        /// <summary>
        /// Force of the gravitiation.
        /// It's really "poor" way to simultate gravity, however it works.
        /// </summary>
        public float GravitationForce = 90f;

        /// <summary>
        /// Movement acceleration limit
        /// Absolute means that it won't additionally be multiplied by delta time.
        /// </summary>
        public float AbsoluteMaxMovementSpeed = 25f;

        /// <summary>
        /// Helper field. 
        /// </summary>
        private bool onPlatform;

        private bool CollidesWith(out List<Tile> colliders)
        {
            colliders = new List<Tile>();

            foreach (var tile in player.game.Tilemap.Tiles)
            {
                if (!tile.Hitbox.IsEmpty && player.Hitbox.Intersects(tile.Hitbox))
                {
                    colliders.Add(tile);
                }
            }
            return colliders.Count > 0;

        }

        /// <summary>
        /// Gravity and jumping helper field.
        /// </summary>
        private bool isInAir = true;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="player">The player.</param>
        public LocalPlayerMovementManager(LocalPlayer player)
        {
            this.player = player;
            game = player.game;
        }

        /// <summary>
        /// Update should be called every frame.
        /// </summary>
        public void Update(KeyboardState keyboardState, GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
        }


        private void HandleHorizontalCollisions()
        {

            if (onPlatform)
                return;

            bool collisionDetected = CollidesWith(out List<Tile> colliders);
            Tile platform = colliders.FirstOrDefault((t) => t.IsAPlatform);
            HorizontalCollisionType collisionType = HorizontalCollisionType.None;

            if (platform != null)
            {
                collisionType = DetermineHorizontalCollisionType(platform.Hitbox);
                CorrectPositionOnHorizontalCollision(platform.Hitbox, collisionType);
            }
        }

        public void HandleHerizontalMovement(KeyboardState keyboardState, float deltaTime)
        {
            if (keyboardState.IsKeyDown(player.Input.Right))
            {
                player.graphicsManager.PlayMoveRightAnimation();
                characterDirection = Direction.Right; //Helps to choose correct animation.

                player.Velocity.X += MovementAcceleration * deltaTime;

                //Speed limit
                if (player.Velocity.X > AbsoluteMaxMovementSpeed)
                    player.Velocity.X = AbsoluteMaxMovementSpeed;

                //If we're changing the move direction we want to reset the velocity on X to make more responsible movement.
                if (player.Velocity.X < 0)
                    player.Velocity.X = 0;

                HandleHorizontalCollisions();

            }
            else if (keyboardState.IsKeyDown(player.Input.Left))
            {
                player.graphicsManager.PlayMoveLeftAnimation();
                characterDirection = Direction.Left; //Helps to choose correct animation.

                player.Velocity.X += -(MovementAcceleration * deltaTime);

                //Speed limit
                if (player.Velocity.X < -AbsoluteMaxMovementSpeed)
                    player.Velocity.X = -AbsoluteMaxMovementSpeed;

                //If we're changing the move direction we want to reset the velocity on X to make more responsible movement.
                if (player.Velocity.X > 0)
                    player.Velocity.X = 0;


                HandleHorizontalCollisions();
            }
            else //No input? - just stop
                player.Velocity.X = 0f;

        }

        public void HandleJumping(KeyboardState keyboardState, float deltaTime)
        {
            bool collisionDetected = CollidesWith(out List<Tile> colliders);
            Tile platform = colliders.FirstOrDefault((t) => t.IsAPlatform);
            VericalCollisionType collisionType = VericalCollisionType.None;

            onPlatform = false; //reset, we gonna set it to right value later.

            OnCollisionEnter(colliders); //Provides mechanics like picking coins etc.
            if (platform != null)
            {
                collisionType = DetermineVerticalCollisionType(platform.Hitbox);
                onPlatform = collisionType == VericalCollisionType.ByTop;

                CorrectPositionOnPlatform(platform.Hitbox, collisionType);
            }


            if (onPlatform)
            {
                isInAir = false;

                player.Velocity.Y = 0f;

            }
            else if (IsAtBottomEdge())
            { 
                isInAir = false;

                player.Velocity.Y = 0f;
                player.Position.Y = game.Graphics.PreferredBackBufferHeight - LocalPlayer.HEIGHT + 1;
            }
            else
                isInAir = true;

            if (keyboardState.IsKeyDown(player.Input.Jump) && !isInAir)
            {
                isInAir = true;
                player.Velocity.Y -= JumpHeight * deltaTime;

                if (characterDirection == Direction.Right)
                    player.graphicsManager.PlayJumpRightAnimation();
                else
                    player.graphicsManager.PlayJumpLeftAnimation();

            }

            if (isInAir)
                player.Velocity.Y += GravitationForce * deltaTime; //Gravitation...

        }


        private bool IsAtBottomEdge()
            => player.Position.Y + LocalPlayer.HEIGHT > game.Graphics.PreferredBackBufferHeight;

        enum VericalCollisionType
        {
            ByTop,
            ByBottom,
            None
        }

        enum HorizontalCollisionType
        {
            FromTheLeft,
            FromTheRight,
            None
        }

        private HorizontalCollisionType DetermineHorizontalCollisionType(Rectangle collider)
        {
            if (player.Hitbox.Right >= collider.Left && player.Hitbox.Left <= collider.Left)
                return HorizontalCollisionType.FromTheRight;
            else if (player.Hitbox.Left <= collider.Right && player.Hitbox.Left >= collider.Left)
                return HorizontalCollisionType.FromTheLeft;
            return HorizontalCollisionType.None;
        }

        private VericalCollisionType DetermineVerticalCollisionType(Rectangle collider)
        {
            //Okey so the coordinate system works a little bit different on Y... Bascially it's just reversed
            if (player.Hitbox.Top <= collider.Bottom && player.Hitbox.Top >= collider.Top)
                return VericalCollisionType.ByBottom;
            else if (player.Hitbox.Bottom >= collider.Top && player.Hitbox.Bottom <= collider.Bottom)
                return VericalCollisionType.ByTop;

            return VericalCollisionType.None;
        }

        private void CorrectPositionOnHorizontalCollision(Rectangle collider, HorizontalCollisionType collisionType)
        {
            if (collisionType == HorizontalCollisionType.FromTheRight)
            {
                player.Velocity.X = 0;
                player.Position.X = collider.Left - LocalPlayer.WIDTH;
            }
            else if (collisionType == HorizontalCollisionType.FromTheLeft)
            {
                player.Velocity.X = 0;
                player.Position.X = collider.Right;
            }
        }

        private void CorrectPositionOnPlatform(Rectangle collider, VericalCollisionType collisionType)
        {

            if (collisionType == VericalCollisionType.ByBottom) //Bottom 
            {
                player.Position.Y = collider.Bottom + LocalPlayer.HEIGHT;
                isInAir = true;
            }
            else if (collisionType == VericalCollisionType.ByTop) //Top
            {
                player.Position.Y = collider.Top - LocalPlayer.HEIGHT + 1;
                isInAir = false;

            }

        }

        private void OnCollisionEnter(List<Tile> colliderTiles)
        {
            if (colliderTiles == null || colliderTiles.Count == 0)
                return;

            foreach (var collider in colliderTiles)
            {
                if (collider.TileType == TileType.Coin)
                {
                    game.Tilemap.Tiles.Remove(collider);
                }
            }
        }
    }
}
